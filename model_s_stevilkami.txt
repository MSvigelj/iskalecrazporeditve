﻿# ureditev kot izmerjena na tipkovnici Das Keyboard 4, brez base key effort
# vrstice, ki se zacnejo z '#' so ignorirane
# prva vrstica dokumenta vsebuje crke katere premikamo locene s presledki, presledek in enter sta impliciran
q w e r t z u i o p š a s d f g h j k l č ž y x c v b n m ć đ , . - ? ! : ;
# st vrstic na tipkovnici
4
# st tipk v vsaki vrstici, loceno z vejico
13,12,12,11
# prazna vrstica

#--------VRSTICA S STEVILKAMI-----------
# vsaka tipka posebej
# pricakuje se ureditev po vrsticah od zgoraj navzdol, od leve proti desni.
# program deluje tudi ce je ureditev drugacna, ni pa pravilen prikaz tipkovnic
#¸
# indeks tipke
0
# pozicije so podane v milimetrih gledano od levega spodnjega roba leve ctrl tipke - x-desno, y-gor, z-navpično (pri nepritisnjeni tipki)
# meri se sredisce tipke
# x pozicija
10
# y pozicija
86
# z pozicija
9
# sirina tipke (torej x dimenzija) v milimetrih
12
# visina tipke (y dimenzija) v milimetrih
14
# roka s katero se tipko pritisne - L ali R
L
# prst s katerim se pritisne tipko 
# (mezinec = 5, prstanec = 4, sredinec = 3, kazalec = 2, palec = 1)
5
# penalty effort effort - osebno prilagojena utez ki ponazarja napor pri pritisku posamezne tipke.
# float stevilka od 0 do 10
3,0
# prazna vrstica

#1
1
29
86
9
12
14
L
5
2,4

#2
2
48
86
9
12
14
L
4
2,2

#3
3
67
86
9
12
14
L
4
2,3

#4
4
86
86
9
12
14
L
3
2,0

#5
5
105
86
9
12
14
L
2
2,2

#6
6
124
86
9
12
14
L
2
2,1

#7
7
143
86
9
12
14
R
2
2,1

#8
8
162
86
9
12
14
R
3
2,0

#9
9
181
86
9
12
14
R
3
2,2

#0
10
200
86
9
12
14
R
4
2,4

#'
11
219
86
9
12
14
R
4
2,4

#+
12
238
86
9
12
14
R
5
2,6

# -------------------- PRVA VRSTICA ----------------
# Q
13
40
65
9
12
14
L
5
1,4

# W
14
59
65
9
12
14
L
4
0,8

#E
15
78
65
9
12
14
L
3
0,8

#R
16
97
65
9
12
14
L
2
1,0

#T
17
116
65
9
12
14
L
2
1,2

#Z
18
135
65
9
12
14
R
2
1,8

#U
19
154
65
9
12
14
R
2
1,2

#I
20
173
65
9
12
14
R
3
0,8

#O
21
192
65
9
12
14
R
4
1,2

#P
22
211
65
9
12
14
R
5
1,6

#Š
23
230
65
9
12
14
R
5
1,8

#Đ
24
249
65
9
12
14
R
5
2,0

# ------------------ DRUGA VRSTICA -----------------
#A
25
46
48
8
12
14
L
5
0,2

#S
26
65
48
8
12
14
L
4
0,2

#D
27
84
48
8
12
14
L
3
0,0

#F
28
103
48
8
12
14
L
2
0,0

#G
29
122
48
8
12
14
L
2
0,8

#H
30
141
48
8
12
14
R
2
0,8

#J
31
160
48
8
12
14
R
2
0,0

#K
32
179
48
8
12
14
R
3
0,0

#L
33
198
48
8
12
14
R
4
0,2

#Č
34
217
48
8
12
14
R
5
0,2

#Ć
35
236
48
8
12
14
R
5
1,0

#Ž
36
255
48
8
12
14
R
5
1,8

#--------------- TRETJA VRSTICA ----------------
#<
37
35
31
8
12
14
L
5
1,0

#Y
38
54
31
8
12
14
L
5
1,0

#X
39
73
31
8
12
14
L
4
1,0

#C
40
92
31
8
12
14
L
3
1,2

#V
41
111
31
8
12
14
L
2
0,6

#B
42
130
31
8
12
14
L
2
2,2

#N
43
149
31
8
12
14
R
2
0,6

#M
44
168
31
8
12
14
R
2
0,6

#,
45
187
31
8
12
14
R
3
0,6

#.
46
206
31
8
12
14
R
4
0,8

#-
47
225
31
8
12
14
R
5
0,4

#[presledek]
48
65
10
8
12
117
R
1
0,0
#po opisu tipke ji lahko dolocimo se fiksirano vrednost
#napise se [fiksirana] [vrednost tipke]
fiksirana presledek

#[enter]
49
275
60
9
25
37
R
5
1,6
fiksirana enter
# po koncu opisa tipk dve prazni vrstici


# za vsak prst posebej se še napiše indeks 'domače' tipke - torej kje prst počiva
# po vrsti - leva roka mezinec, prstanec, sredinec, kazalec; desna roka - kazalec, sredinec, prstanec, mezinec
# palci zaenkrat fiksirani na presledku - možnost nadgradnje
25 26 27 28 31 32 33 34