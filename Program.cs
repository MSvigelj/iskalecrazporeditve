﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;

namespace ProgramZaDiplomsko {
    class Program {
        static void Main(string[] args) {
            Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.GetCultureInfo("sl");
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            Ureditev u = new Ureditev();
            string[] korpus;
            Parametri p = new Parametri();
            int[,,] steviloTriad;
            

            if (args.Length > 0 && args[0].Equals("--help")) {
                IzpisiPomoc();
                return;
            }
            else if(args.Length == 3) {
                if(File.Exists(args[0]) && File.Exists(args[1]) && Directory.Exists(args[2])) {
                    u.PreberiUreditev(args[0]);
                    Console.WriteLine();
                    Console.WriteLine("Začenjam s prebiranjem parametrov. ");
                    p = PreberiParametre(args[1]);
                    Console.WriteLine();
                    Console.WriteLine("Začenjam s prebiranjem korpusa. ");
                    korpus = ImportajKorpus(args[2], 30000, stevilke: u.stevilke, locila: u.locila);
                    Console.WriteLine("Stevilo prebranih elementov za korpus: " + korpus.Length);
                    Console.WriteLine("Začenjam analizo korpusa. ");
                    steviloTriad = PrestejTriade(u, korpus);
                    korpus = null;
                    Console.WriteLine("Končal z analizo korpusa. ");
                }
                else {
                    IzpisiPomoc();
                    return;
                }
            }
            else {
                IzpisiPomoc();
                return;
            }

            GenetskiAlgoritem ga = new GenetskiAlgoritem(u, steviloTriad, p, velikostPopulacije: 3000, procentStarsev: 0.60f, moznostMutacije: 0.035f, maxSteviloIteracij: 300);
            
        }

        static void IzpisiPomoc() {
            Console.WriteLine("Uporaba programa: ");
            Console.WriteLine("layoutFinder [ureditev] [parametri] [korpus]");
            Console.WriteLine("[razporeditev] in [parametri] sta poti do tekstovne datoteke v kateri je: 1. definirana fizična ureditev tipkovnice za katero " +
                "iščete postavitev črk, 2. definirani parametri za algoritem. ");
            Console.WriteLine("[korpus] je pot do direktorija s datotekami korpusa. Obvezno *.txt datoteke. ");
            
        }

        //path je pot do direktorija v katerem so korpus datoteke (obvevzno .txt), stFilov doloca st prebranih datotek,
        //ce je vrednost stFilov -1, se prebere vse datoteke v direktoriju.
        static string[] ImportajKorpus(string path, int stFilov, bool stevilke = false, bool locila = false) {
            string[] filePaths = Directory.GetFiles(path, "*.txt", SearchOption.TopDirectoryOnly);
            
            if(stFilov == -1) {
                stFilov = filePaths.Length;
            }
            else if(stFilov > filePaths.Length) {
                stFilov = filePaths.Length;
            }
            string[] korpus = new string[stFilov];
            for (int i = 0; i < stFilov; i++) {
                korpus[i] = PrecistiString(File.ReadAllText(filePaths[i]), stevilke, locila);
            }

            return korpus;
        }

        //Precisti string, da v njemu ostanejo samo relavantni znaki. 
        static string PrecistiString(string a, bool stevilke = false, bool locila = false) {
            StringBuilder sb = new StringBuilder();
            a = a.ToLower();

            bool prejsnjiJePresledek = false;
            foreach (char c in a) {
                if(stevilke && Char.IsNumber(c)) {
                    sb.Append(c);
                    prejsnjiJePresledek = false;
                }
                else if(locila && Char.IsPunctuation(c)) {
                    sb.Append(c);
                    prejsnjiJePresledek = false;
                }
                else if(c.Equals('\n') || c.Equals(' ')) {
                    if (!prejsnjiJePresledek) {
                        sb.Append(c);
                    }
                    prejsnjiJePresledek = true;
                }
                else if (Char.IsLetter(c)) {
                    sb.Append(c);
                    prejsnjiJePresledek = false;
                }
            }
            a = sb.ToString();
            return a;
        }

        public static Parametri PreberiParametre(string path) {
            Parametri p = new Parametri();
            string[] s = File.ReadAllLines(path);
            s = Utilities.IzbrisiKomentarje(s);

            foreach(string vrstica in s) {
                if(vrstica.Length == 0) { //preskakujem prazne vrstice
                    continue;
                }
                string[] split = vrstica.Split('=');

                if (split[0].Equals("penaltyEffortMultiplier")) {
                    try {
                        p.penaltyEffortMultiplier = float.Parse(split[1]);
                    }
                    catch {
                        Console.WriteLine("Napaka v formatu parametrov. ");
                        Console.WriteLine("Vrstica: " + vrstica);
                        Environment.Exit(1);
                    }
                }
                else if (split[0].Equals("baseEffortMultiplier")) {
                    try {
                        p.baseEffortMultiplier = float.Parse(split[1]);
                    }
                    catch {
                        Console.WriteLine("Napaka v formatu parametrov. ");
                        Console.WriteLine("Vrstica: " + vrstica);
                        Environment.Exit(1);
                    }
                }
                else if (split[0].Equals("key1Multiplier")) {
                    try {
                        p.key1Multiplier = float.Parse(split[1]);
                    }
                    catch {
                        Console.WriteLine("Napaka v formatu parametrov. ");
                        Console.WriteLine("Vrstica: " + vrstica);
                        Environment.Exit(1);
                    }
                }
                else if (split[0].Equals("key2Multiplier")) {
                    try {
                        p.key2Multiplier = float.Parse(split[1]);
                    }
                    catch {
                        Console.WriteLine("Napaka v formatu parametrov. ");
                        Console.WriteLine("Vrstica: " + vrstica);
                        Environment.Exit(1);
                    }
                }
                else if (split[0].Equals("key3Multiplier")) {
                    try {
                        p.key3Multiplier = float.Parse(split[1]);
                    }
                    catch {
                        Console.WriteLine("Napaka v formatu parametrov. ");
                        Console.WriteLine("Vrstica: " + vrstica);
                        Environment.Exit(1);
                    }
                }
                else if (split[0].Equals("strokeEffortMultiplier")) {
                    try {
                        p.strokeEffortMultiplier = float.Parse(split[1]);
                    }
                    catch {
                        Console.WriteLine("Napaka v formatu parametrov. ");
                        Console.WriteLine("Vrstica: " + vrstica);
                        Environment.Exit(1);
                    }
                }
                else if (split[0].Equals("handEffortMultiplier")) {
                    try {
                        p.handEffortMultiplier = float.Parse(split[1]);
                    }
                    catch {
                        Console.WriteLine("Napaka v formatu parametrov. ");
                        Console.WriteLine("Vrstica: " + vrstica);
                        Environment.Exit(1);
                    }
                }
                else if (split[0].Equals("rowEffortMultiplier")) {
                    try {
                        p.rowEffortMultiplier = float.Parse(split[1]);
                    }
                    catch {
                        Console.WriteLine("Napaka v formatu parametrov. ");
                        Console.WriteLine("Vrstica: " + vrstica);
                        Environment.Exit(1);
                    }
                }
                else if (split[0].Equals("fingerEffortMultiplier")) {
                    try {
                        p.fingerEffortMultiplier = float.Parse(split[1]);
                    }
                    catch {
                        Console.WriteLine("Napaka v formatu parametrov. ");
                        Console.WriteLine("Vrstica: " + vrstica);
                        Environment.Exit(1);
                    }
                }
                else if (split[0].Equals("izmenjajoce")) {
                    try {
                        p.izmenjajoce = float.Parse(split[1]);
                    }
                    catch {
                        Console.WriteLine("Napaka v formatu parametrov. ");
                        Console.WriteLine("Vrstica: " + vrstica);
                        Environment.Exit(1);
                    }
                }
                else if (split[0].Equals("neizmenjajoce")) {
                    try {
                        p.neizmenjajoce = float.Parse(split[1]);
                    }
                    catch {
                        Console.WriteLine("Napaka v formatu parametrov. ");
                        Console.WriteLine("Vrstica: " + vrstica);
                        Environment.Exit(1);
                    }
                }
                else if (split[0].Equals("samoEnaRoka")) {
                    try {
                        p.samoEnaRoka = float.Parse(split[1]);
                    }
                    catch {
                        Console.WriteLine("Napaka v formatu parametrov. ");
                        Console.WriteLine("Vrstica: " + vrstica);
                        Environment.Exit(1);
                    }
                }
                else if (split[0].Equals("vsiRazlicni")) {
                    try {
                        p.vsiRazlicni = float.Parse(split[1]);
                    }
                    catch {
                        Console.WriteLine("Napaka v formatu parametrov. ");
                        Console.WriteLine("Vrstica: " + vrstica);
                        Environment.Exit(1);
                    }
                }
                else if (split[0].Equals("polPolPonavljanje")) {
                    try {
                        p.polPolPonavljanje = float.Parse(split[1]);
                    }
                    catch {
                        Console.WriteLine("Napaka v formatu parametrov. ");
                        Console.WriteLine("Vrstica: " + vrstica);
                        Environment.Exit(1);
                    }
                }
                else if (split[0].Equals("razlicniRolling")) {
                    try {
                        p.razlicniRolling = float.Parse(split[1]);
                    }
                    catch {
                        Console.WriteLine("Napaka v formatu parametrov. ");
                        Console.WriteLine("Vrstica: " + vrstica);
                        Environment.Exit(1);
                    }
                }
                else if (split[0].Equals("razlicniNemonotoni")) {
                    try {
                        p.razlicniNemonotoni = float.Parse(split[1]);
                    }
                    catch {
                        Console.WriteLine("Napaka v formatu parametrov. ");
                        Console.WriteLine("Vrstica: " + vrstica);
                        Environment.Exit(1);
                    }
                }
                else if (split[0].Equals("polPolIzmenjaje")) {
                    try {
                        p.polPolIzmenjaje = float.Parse(split[1]);
                    }
                    catch {
                        Console.WriteLine("Napaka v formatu parametrov. ");
                        Console.WriteLine("Vrstica: " + vrstica);
                        Environment.Exit(1);
                    }
                }
                else if (split[0].Equals("vsiIstiPonavljanje")) {
                    try {
                        p.vsiIstiPonavljanje = float.Parse(split[1]);
                    }
                    catch {
                        Console.WriteLine("Napaka v formatu parametrov. ");
                        Console.WriteLine("Vrstica: " + vrstica);
                        Environment.Exit(1);
                    }
                }
                else if (split[0].Equals("polPolZapored")) {
                    try {
                        p.polPolZapored = float.Parse(split[1]);
                    }
                    catch {
                        Console.WriteLine("Napaka v formatu parametrov. ");
                        Console.WriteLine("Vrstica: " + vrstica);
                        Environment.Exit(1);
                    }
                }
                else if (split[0].Equals("vsiIstiBrezPonavljanja")) {
                    try {
                        p.vsiIstiBrezPonavljanja = float.Parse(split[1]);
                    }
                    catch {
                        Console.WriteLine("Napaka v formatu parametrov. ");
                        Console.WriteLine("Vrstica: " + vrstica);
                        Environment.Exit(1);
                    }
                }
                else {
                    Console.WriteLine("Napaka v formatu parametrov. ");
                    Console.WriteLine("Vrstica: " + vrstica);
                    Environment.Exit(1);
                }

            }

            Console.WriteLine("Prebral parametre. Parametri so sledeči: ");
            Console.WriteLine("penaltyEffortMultiplier: " + p.penaltyEffortMultiplier);
            Console.WriteLine("baseEffortMultiplier: " + p.baseEffortMultiplier);
            Console.WriteLine("key1Multiplier: " + p.key1Multiplier);
            Console.WriteLine("key2Multiplier: " + p.key2Multiplier);
            Console.WriteLine("key3Multiplier: " + p.key3Multiplier);
            Console.WriteLine("strokeEffortMultiplier: " + p.strokeEffortMultiplier);
            Console.WriteLine("handEffortMultiplier: " + p.handEffortMultiplier);
            Console.WriteLine("rowEffortMultiplier: " + p.rowEffortMultiplier);
            Console.WriteLine("fingerEffortMultiplier: " + p.fingerEffortMultiplier);
            Console.WriteLine("izmenjajoce: " + p.izmenjajoce);
            Console.WriteLine("neizmenjajoce: " + p.neizmenjajoce);
            Console.WriteLine("samoEnaRoka: " + p.samoEnaRoka);
            Console.WriteLine("vsiRazlicni: " + p.vsiRazlicni);
            Console.WriteLine("polPolPonavljanje: " + p.polPolPonavljanje);
            Console.WriteLine("razlicniRolling: " + p.razlicniRolling);
            Console.WriteLine("razlicniNemonotoni: " + p.razlicniNemonotoni);
            Console.WriteLine("polPolIzmenjaje: " + p.polPolIzmenjaje);
            Console.WriteLine("vsiIstiPonavljanje: " + p.vsiIstiPonavljanje);
            Console.WriteLine("polPolZapored: " + p.polPolZapored);
            Console.WriteLine("vsiIstiBrezPonavljanja: " + p.vsiIstiBrezPonavljanja);

            return p;
        }

        /// <summary>
        /// Presteje stevilo pojavitev vsake mogoce triade v nasem naboru.
        /// Triade sestavljene iz crk, katerih ne iscemo, se ignorira.
        /// </summary>
        private static int[,,] PrestejTriade(Ureditev ureditev, string[] korpus) {
            int[,,] steviloTriad = new int[
                ureditev.setCrk.Length + ureditev.fiksneTipke.Length,
                ureditev.setCrk.Length + ureditev.fiksneTipke.Length,
                ureditev.setCrk.Length + ureditev.fiksneTipke.Length];
            short[] triada = new short[3];

            foreach (string s in korpus) {//za vsak vnos v korpusu
                for (int i = 0; i < s.Length - 2; i++) {//gremo cez cel vnos po triadah
                    for (int j = 0; j < 3; j++) {
                        triada[j] = ureditev.Vrednost(s[i + j]);
                    }

                    if (triada[2] == -1 || triada[1] == -1 || triada[0] == -1) { //preskakovanje triad z neveljavnimi crkami
                        i += 2;
                        continue;
                    }

                    steviloTriad[triada[0], triada[1], triada[2]]++;
                }
            }
            return steviloTriad;
        }

    }

    public class Parametri {
        public float penaltyEffortMultiplier;
        public float baseEffortMultiplier;
        public float key1Multiplier;
        public float key2Multiplier;
        public float key3Multiplier;
        public float strokeEffortMultiplier;
        public float handEffortMultiplier;
        public float rowEffortMultiplier;
        public float fingerEffortMultiplier;
        public float izmenjajoce;
        public float neizmenjajoce;
        public float samoEnaRoka;
        public float vsiRazlicni;
        public float polPolPonavljanje;
        public float razlicniRolling;
        public float razlicniNemonotoni;
        public float polPolIzmenjaje;
        public float vsiIstiPonavljanje;
        public float polPolZapored;
        public float vsiIstiBrezPonavljanja;

        public Parametri() {
            penaltyEffortMultiplier = 3.1f;
            baseEffortMultiplier = 1.4f;
            key1Multiplier = 1.0f;
            key2Multiplier = 0.3f;
            key3Multiplier = 0.3f;
            strokeEffortMultiplier = 0.7f;
            handEffortMultiplier = 3.0f;
            rowEffortMultiplier = 0.8f;
            fingerEffortMultiplier = 3.0f;
            izmenjajoce = 0.0f;
            neizmenjajoce = 1.0f;
            samoEnaRoka = 1.7f;
            vsiRazlicni = 0.0f;
            polPolPonavljanje = 0.3f;
            razlicniRolling = 0.7f;
            razlicniNemonotoni = 1.0f;
            polPolIzmenjaje = 2.6f;
            vsiIstiPonavljanje = 3.8f;
            polPolZapored = 3.4f;
            vsiIstiBrezPonavljanja = 6.5f;
    }
    }

    public class GenetskiAlgoritem {

        int velikostPopulacije;
        int maxSteviloIteracij; //-1 pomeni neomejeno
        float procentStarsev;
        float moznostMutacije;
        Ureditev ureditev;
        Kromosom[] populacija;
        int[,,] steviloTriad;
        Parametri p;
        Kromosom najboljsiRezultat;

        public GenetskiAlgoritem(Ureditev ureditev, int[,,] steviloTriad, Parametri p, int velikostPopulacije = 1000, float procentStarsev = 0.1f, float moznostMutacije = 0.05f, int maxSteviloIteracij = -1) {
            this.velikostPopulacije = velikostPopulacije;
            this.ureditev = ureditev;
            this.procentStarsev = procentStarsev;
            this.moznostMutacije = moznostMutacije;
            this.maxSteviloIteracij = maxSteviloIteracij;
            this.steviloTriad = steviloTriad;
            this.p = p;
            this.najboljsiRezultat = new Kromosom(ureditev.setCrk.Length, (short)ureditev.tipke.Length);
            najboljsiRezultat.fitnes = float.MaxValue;

            Console.WriteLine();
            Console.WriteLine("Začenjam z iskanjem. ");
            GenerirajPopulacijo();

            int trenutnaGeneracija = 1;
            while(true)  {
                if (trenutnaGeneracija == this.maxSteviloIteracij+1)
                    break;
                Console.WriteLine("Trenutna generacija: " + trenutnaGeneracija);
                if (!Iteracija())
                    break;
                trenutnaGeneracija++;
            }
        }

        bool Iteracija() {
            EvaluirajPopulacijo();
            Array.Sort(populacija);
            if(populacija[0].fitnes < najboljsiRezultat.fitnes) {
                najboljsiRezultat = populacija[0].Clone();
            }
            IzpisiFitnesPopulacije(izpisiNajboljsega: true);

            GenerirajNovoPopulacijo();
            return true;
        }

        void GenerirajNovoPopulacijo() {
            Kromosom[] novaPopulacija = new Kromosom[velikostPopulacije];
            Kromosom[] starsi = Selekcija(elitizem: 0.03f);
            for(int i = 0; i < velikostPopulacije; i++) {
                novaPopulacija[i] = NovKromosom(starsi);
            }

            populacija = novaPopulacija;
        }

        Kromosom NovKromosom(Kromosom[] starsi) {
            Random r = new Random();
            Kromosom prvi = starsi[r.Next(starsi.Length)];
            Kromosom drugi = starsi[r.Next(starsi.Length)];

            return Mutiraj(Krizaj(prvi, drugi));
        }

        Kromosom Krizaj(Kromosom prvi, Kromosom drugi, int nacinKrizanja = 1) {

            Kromosom novi = new Kromosom();
            novi.tipkovnica = new short[prvi.tipkovnica.Length];

            //problem je, ker niso vsi elementi unikatni, ker so ene tipke lahko prazne
            //posledicno rabim vedeti koliko je praznih in potem implementirat spremembe,
            //da funkcije vseeno delajo
            int stPraznih = 0;
            for(int i = 0; i < prvi.tipkovnica.Length; i++) {
                if (prvi.tipkovnica[i] == -1)
                    stPraznih++;
            }

            //v ifih razlicni nacini krizanja

            //order crossover
            if(nacinKrizanja == 0) {

                //izrezemo del prvega starsa
                Random r = new Random();
                int start = r.Next(0, novi.tipkovnica.Length - 1); //minus ena zato, da je vedno vsaj element iz prvega starsa
                int end = r.Next(start + 1, novi.tipkovnica.Length); //isti razlog za start + 1
                for(int i = start; i < end; i++) {
                    novi.tipkovnica[i] = prvi.tipkovnica[i];
                    if (prvi.tipkovnica[i] == -1) //dodatek za stetje praznih polj
                        stPraznih--;
                }

                //gremo cez array, za vsak odprt prostor prekopiramo vrednost iz 2ega starsa, 
                //ce pa je ta vrednost ze uporabljena, prekopiramo naslednjo
                int p2Idx = 0;
                for(int i = 0; i < novi.tipkovnica.Length; i++) {
                    if(i == start) { //preskocim ze zaseden del
                        i = end;
                    }

                    while (true) {
                        if (drugi.tipkovnica[p2Idx] == -1 && stPraznih > 0) {
                            break; //breakamo ce je element prazen in imamo se na razpolago prazne
                        }//ali pa
                        else if (Utilities.Najdi(novi.tipkovnica, drugi.tipkovnica[p2Idx], start, end) == -1) {
                            break; //ce element ni vsebovan v prekopiranih elementih
                        }
                        p2Idx++;
                    }

                    //prekopiram element
                    novi.tipkovnica[i] = drugi.tipkovnica[p2Idx];
                    if (drugi.tipkovnica[p2Idx] == -1)
                        stPraznih--;
                    p2Idx++;
                }
            }

            //moja verzija crossoverja, ki najbrz ze nekje obstaja pa ne vem kako se klice
            if(nacinKrizanja == 1) {
                //izrezemo del prvega starsa
                Random r = new Random();
                int start = r.Next(0, novi.tipkovnica.Length - 1); //minus ena zato, da je vedno vsaj element iz prvega starsa
                int end = r.Next(start + 1, novi.tipkovnica.Length); //isti razlog za start + 1
                for (int i = start; i < end; i++) {
                    novi.tipkovnica[i] = prvi.tipkovnica[i];
                    if (prvi.tipkovnica[i] == -1) //dodatek za stetje praznih polj
                        stPraznih--;
                }

                int startDrugi = start;

                for(int i = 0; i < novi.tipkovnica.Length; i++) { //nafilamo cel array z vrednostmi drugega starsa

                    if (i == start) { //preskocim ze zaseden del
                        i = end;
                    }

                    //ce vrednosti na tem mestu se ni v novem arrayu, jo kopiramo
                    if (Utilities.Najdi(novi.tipkovnica, drugi.tipkovnica[i], start, end) == -1) {
                        novi.tipkovnica[i] = drugi.tipkovnica[i];
                        continue;
                    }

                    //ce je vrednost -1 in mamo se praznih na zalogi, kopiramo
                    if(drugi.tipkovnica[i] == -1 && stPraznih > 0) {
                        novi.tipkovnica[i] = drugi.tipkovnica[i];
                        stPraznih--;
                        continue;
                    }

                    //drugace pa skopiramo naslednji element ki se ni uporabljen, in tudi ne bo na svoji poziciji
                    //ti elementi so vedno znotraj tistega prostora iz kjer smo prekopirali elemente prvega starsa
                    bool nasel = false;
                    for(; startDrugi < end; startDrugi++) {
                        
                        if (Utilities.Najdi(novi.tipkovnica, drugi.tipkovnica[startDrugi], start, end) == -1) { //takega elementa se ni
                            novi.tipkovnica[i] = drugi.tipkovnica[startDrugi];
                            startDrugi++;
                            nasel = true;
                            break;
                        }
                        else if(drugi.tipkovnica[startDrugi] == -1 && stPraznih > 0) { //prazno polje
                            stPraznih--;
                            novi.tipkovnica[i] = drugi.tipkovnica[startDrugi];
                            startDrugi++;
                            nasel = true;
                            break;
                        }
                    }

                    if (!nasel) {
                        Console.WriteLine("Napaka v crossover = 1 funkciji. ");
                    }
                }
            }

            return novi;
        }

        Kromosom Mutiraj(Kromosom k) {
            int stMutacij = (int)(moznostMutacije * k.tipkovnica.Length);
            Random r = new Random();
            for(int i = 0; i < stMutacij; i++) {
                int idx1 = r.Next(k.tipkovnica.Length);
                int idx2 = r.Next(k.tipkovnica.Length);

                short temp = k.tipkovnica[idx1];
                k.tipkovnica[idx1] = k.tipkovnica[idx2];
                k.tipkovnica[idx2] = temp;
            }

            return k;
        }

        //Funkcija za selekcijo. Trenutno implementirana samo stochastic universal sampling.
        Kromosom[] Selekcija(int tip = 0 , float elitizem = 0f) {
            Random r = new Random();
            //ker pri meni manjsi fitness pomeni boljsi gen, moram to najprej obrniti
            //ideja je: fitnesEnega = (fitnesMax + fitnesAvg) - orgFitnesEnega
            //ker je selekcija fitness proportionate bom si namesto da dejansko spreminjam fitnes posameznikov
            //raje naredil nov array s proporcijami
            Kromosom[] izbrani = new Kromosom[(int)(populacija.Length * procentStarsev)];

            int stElitizma = (int)(elitizem * populacija.Length);
            int izbraniIdx = 0;
            for(; izbraniIdx < stElitizma; izbraniIdx++) {
                izbrani[izbraniIdx] = populacija[izbraniIdx];
            }


            float fitnesMax = populacija[populacija.Length - 1].fitnes;
            double fitnesSum = 0;

            foreach(Kromosom k in populacija) {
                fitnesSum += k.fitnes;
            }

            float fitnesAvg = (float)(fitnesSum / populacija.Length);
            float avgMax = fitnesAvg + fitnesMax;

            double sum = 0;
            float[] proporcijonalniFitnes = new float[populacija.Length];
            for(int i = 0; i < populacija.Length; i++) {
                proporcijonalniFitnes[i] = avgMax - populacija[i].fitnes;
                sum += proporcijonalniFitnes[i];
            }

            for (int i = 0; i < populacija.Length; i++) {
                proporcijonalniFitnes[i] = (float)(proporcijonalniFitnes[i]/sum);
            }

            //stochastic universal sampling
            if (tip == 0) {
                float razdalja = 1.0f / (izbrani.Length - stElitizma);
                float start = (float)r.NextDouble() * razdalja;

                int fitnesIdx = 0;
                fitnesSum = 0;

                
                for(; izbraniIdx < izbrani.Length; izbraniIdx++, start+=razdalja) {
                    for(;fitnesIdx < proporcijonalniFitnes.Length-1; fitnesSum += proporcijonalniFitnes[fitnesIdx], fitnesIdx++) {
                        if (fitnesSum + proporcijonalniFitnes[fitnesIdx] >= start)
                            break;
                    }

                    izbrani[izbraniIdx] = populacija[fitnesIdx];
                }
                
            }

            return izbrani;

        }

        public void GenerirajPopulacijo() {
            populacija = new Kromosom[velikostPopulacije];
            for (int i = 0; i < velikostPopulacije; i++)
                populacija[i] = new Kromosom(ureditev.setCrk.Length, (short)ureditev.tipke.Length);
            
        }

        public void EvaluirajPopulacijo() {
            foreach(Kromosom k in populacija) {
                k.IzracunajFitnes(steviloTriad, ureditev, this.p);
            }
        }

        public void IzpisiFitnesPopulacije(bool izpisiCelotnoPopulacijo = false, bool izpisiNajboljsega = false) {

            double sumPopulacije = 0;
            foreach(Kromosom k in populacija) {
                sumPopulacije += k.fitnes;
            }
            Console.WriteLine("Povprecen fitnes populacije: " + sumPopulacije / populacija.Length);
            if (izpisiCelotnoPopulacijo) {
                Console.Write("[");
                foreach (Kromosom k in populacija) {
                    Console.Write(k.fitnes + ", ");
                }
                Console.WriteLine("]");
            }

            if (izpisiNajboljsega) {
                Console.WriteLine("Najboljsa ureditev: ");
                ureditev.IzpisiUreditev(najboljsiRezultat);
                najboljsiRezultat.IzpisiStatistiko();
            }       
        }
    }

    public static class Utilities {

        //vrne idx iskanega objekta oziroma -1, ce ga ni
        //opcijski argumenti kje zacne in kje konca [) iskanje
        public static int Najdi(Array a, object iskan, int start = 0, int end = -1) {
            if(end == -1) {
                end = a.Length;
            }
            for(int i = start; i < end; i++) {
                if (a.GetValue(i).Equals(iskan))
                    return i;
            }

            return -1;
        }

        public static string[] IzbrisiKomentarje(string[] sKomentarji) {
            List<string> brezKomentarjev = new List<string>();

            foreach (string s in sKomentarji) {
                if (s.Length == 0) {
                    brezKomentarjev.Add(s);
                    continue;
                }

                if (s[0] == '#')
                    continue;

                brezKomentarjev.Add(s);
            }

            return brezKomentarjev.ToArray();
        }
    }

    public class Kromosom : IComparable<Kromosom> {
        public short[] tipkovnica; //array v velikosti kolikor je nefiksnih tipk na tipkovnici. idx v arrayu pomeni idx na tipkovnici. st v array pomeni idx crke. -1 je prazen prostor
        public float fitnes;

        public float averageBaseEffort;
        public float averagePenaltyEffort;
        public float averageStrokeEffort;
        public float averageHandEffort;
        public float averageRowEffort;
        public float averageFingerEffort;


        //vrne kromosom z nakljucno ureditvijo crk
        public Kromosom(int stCrk, short stTipk) {
            this.tipkovnica = new short[stCrk];


            short[] a = ArrayZaporednihStevil(stCrk, stTipk);
            a = PremesajArray(a);
            this.tipkovnica = a;
            return;
        }

        public Kromosom() {
            return;
        }

        public Kromosom Clone() {
            Kromosom k = new Kromosom();
            k.tipkovnica = new short[tipkovnica.Length];
            for(int i = 0; i < tipkovnica.Length; i++) {
                k.tipkovnica[i] = this.tipkovnica[i];
            }
            k.fitnes = this.fitnes;
            k.averageBaseEffort = this.averageBaseEffort;
            k.averagePenaltyEffort = this.averagePenaltyEffort;
            k.averageStrokeEffort = this.averageStrokeEffort;
            k.averageHandEffort = this.averageHandEffort;
            k.averageRowEffort = this.averageRowEffort;
            k.averageFingerEffort = this.averageFingerEffort;

            return k;
        }

        public void IzracunajFitnes(
            int[,,] steviloTriad,
            Ureditev u,
            Parametri p) {


            float[,,] effortTriad = new float[steviloTriad.GetLength(0), steviloTriad.GetLength(0), steviloTriad.GetLength(0)];
            float[,,] penaltyEffort = new float[steviloTriad.GetLength(0), steviloTriad.GetLength(0), steviloTriad.GetLength(0)];
            float[,,] baseEffort = new float[steviloTriad.GetLength(0), steviloTriad.GetLength(0), steviloTriad.GetLength(0)];
            float[,,] strokeEffort = new float[steviloTriad.GetLength(0), steviloTriad.GetLength(0), steviloTriad.GetLength(0)];
            //hand row in finger effort so tri podskupine za racunanje stroke efforta
            float[,,] handEffort = new float[steviloTriad.GetLength(0), steviloTriad.GetLength(0), steviloTriad.GetLength(0)];
            float[,,] rowEffort = new float[steviloTriad.GetLength(0), steviloTriad.GetLength(0), steviloTriad.GetLength(0)];
            float[,,] fingerEffort = new float[steviloTriad.GetLength(0), steviloTriad.GetLength(0), steviloTriad.GetLength(0)];
            double sum = 0;
            long skupnoSteviloTriad = 0;

            //za debug
            double sumPenalty = 0;
            double sumBaseEffort = 0;
            double sumStroke = 0;
            double sumHandEffort = 0;
            double sumRowEffort = 0;
            double sumFingerEffort = 0;

            int stPraznihTriad = 0;

            for (short a = 0; a < u.setCrk.Length + u.fiksneTipke.Length; a++) {
                for (short b = 0; b < u.setCrk.Length + u.fiksneTipke.Length; b++) {
                    for (short c = 0; c < u.setCrk.Length + u.fiksneTipke.Length; c++) {
                        if (steviloTriad[a, b, c] == 0) { //ne racunamo vrednosti triad, ki se ne pojavijo v korpusu
                            stPraznihTriad++;
                            continue;
                        }


                        Tipka aT = u.PridobiTipko(PridobiIdxTipke(a, u));
                        Tipka bT = u.PridobiTipko(PridobiIdxTipke(b, u));
                        Tipka cT = u.PridobiTipko(PridobiIdxTipke(c, u));

                        //formule kot opisane na carpalxu, ideja je da zaporedne tezke tipke stanejo vec
                        penaltyEffort[a,b,c] = (aT.penaltyEffort * p.key1Multiplier * (1 + bT.penaltyEffort * p.key2Multiplier * (1 + cT.penaltyEffort * p.key3Multiplier))) * p.penaltyEffortMultiplier; //najprej penalty efforti
                        baseEffort[a,b,c] = (aT.baseEffort * p.key1Multiplier * (1 + bT.baseEffort * p.key2Multiplier * (1 + cT.baseEffort * p.key3Multiplier))) * p.baseEffortMultiplier; ; //base efforti

                        //hand effort
                        //te vrednosti bi se potencialno tudi lahko prebralo iz nekega fila, ampak zaenkrat naj bodo take kakrsne so v carpalxu
                        //0 - izmenjajoce  -- 0 in 1 sem zamenjal v primerjavi s carpalxom
                        //1 - neizmenjajoce
                        //2 - samo ena
                        float izmenjajoce = p.izmenjajoce;
                        float neizmenjajoce = p.neizmenjajoce;
                        float samoEnaRoka = p.samoEnaRoka;
                        float hE;
                        if (aT.levaRoka == cT.levaRoka) {
                            if (bT.levaRoka == cT.levaRoka) {//vse natipka ena roka
                                hE = samoEnaRoka;
                            }
                            else {//izmenjujejo se roke
                                hE = izmenjajoce;
                            }
                        }
                        else {//bodisi prvi dve ali pa zadnji dve crki napise ista roka
                            hE = neizmenjajoce;
                        }

                        handEffort[a,b,c] = hE * p.handEffortMultiplier;

                        //row effort
                        //namesto vnaprej definiranih utezi, se za utez vzame skupno vertikalno distanco med tipkami.
                        //to posledicno pomeni, da je tipkanje na eni vrstici lazje kot na vecih razlicnih (kar tako ali tako doseze tudi carplaxov model),
                        //da so zaporedne tipke na dveh vrsticah ki so mocno narazen (npr. najprej spodnja vrstica, za njo pa zgornja) primerno kaznovane in 
                        //da je model posplosljiv tudi na tipkovnice z vec kot 3emi vrsticami
                        //tudi to pa po mojem mnenju ni idealno, ker ce se roki izmenjujeta potem pozicija tipk po vrsticah niti nima veze
                        rowEffort[a,b,c] = (Math.Abs(aT.y - bT.y) + Math.Abs(bT.y - cT.y)) / 10 * p.rowEffortMultiplier;

                        //finger effort
                        //0 - vsi razlicni, premik v eno smer ahl: ajk, kja, pua, asd
                        //1 - pol pol, ponavljanje: ajj, dpp, ssk
                        //2 - razlicni, rolling - dve zaporedni tipki z isto roko navznoter
                        //3 - razlicni, nemonotoni (ne v isto smer): yak, nep
                        //4 - pol pol, izmenjaje: kri, maj
                        //5 - vsi isti, ponavljanje: cee, loo, juu
                        //6 - pol pol, ponavljajoci zapored, monotoni: abr, bde 
                        //7 - vsi isti, brez ponavljanja: tfb, dec

                        // Potencialno se implementira uporabnikov vnos.
                        float vsiRazlicni = p.vsiRazlicni;
                        float polPolPonavljanje = p.polPolPonavljanje;
                        float razlicniRolling = p.razlicniRolling;
                        float razlicniNemonotoni = p.razlicniNemonotoni;
                        float polPolIzmenjaje = p.polPolIzmenjaje;
                        float vsiIstiPonavljanje = p.vsiIstiPonavljanje;
                        float polPolZapored = p.polPolZapored;
                        float vsiIstiBrezPonavljanja = p.vsiIstiBrezPonavljanja;
                        float fE = -1;


                        //mislim da je to prav, ampak realno ne morem preverit vseh 15k kombinacij
                        if (aT.prst != bT.prst || aT.levaRoka != bT.levaRoka) {//prva dva razlicna
                            if ((aT.prst != cT.prst || aT.levaRoka != cT.levaRoka) && (bT.prst != cT.prst || bT.levaRoka != cT.levaRoka)) { //vsi razlicni
                                //opcije
                                //monotono - 0
                                //rolling - 2
                                //nemonotono - 3

                                if (aT.levaRoka) {
                                    if (bT.levaRoka) {
                                        if (aT.prst > bT.prst) { //zaenkrat monotono leva > desni, vsaj rolling
                                            if (cT.levaRoka) {
                                                if (bT.prst > cT.prst) { //monotono leva > desni vse leva roka
                                                    fE = vsiRazlicni;
                                                }
                                                else { //rolling (prve dve so rolling
                                                    fE = razlicniRolling;
                                                }
                                            }
                                            else { //monotono leva > desni, zadnja crka v desni roki
                                                fE = vsiRazlicni;
                                            }
                                        }
                                        else { //prve dve crke greste navzven v levi roki
                                            if (cT.levaRoka) {
                                                if (cT.prst > bT.prst) {//monotono, desna > levi
                                                    fE = vsiRazlicni;
                                                }
                                                else {//zadnje dve crke mate rolling
                                                    fE = razlicniRolling;
                                                }
                                            }
                                            else {//nemonotono, vsi razlicni
                                                fE = razlicniNemonotoni;
                                            }
                                        }
                                    }
                                    else { //druga crka v desni roki
                                        if (!cT.levaRoka) {//tretja crka tudi v desni roki
                                            if (cT.prst > bT.prst) { //monotono leva > desni
                                                fE = vsiRazlicni;
                                            }
                                            else { //rolling v desni roki
                                                fE = razlicniRolling;
                                            }
                                        }
                                        else {//nemonotono (izmenjavanje rok)
                                            fE = razlicniNemonotoni;
                                        }
                                    }
                                }

                                else { //prva crka v desni roki
                                    if (!bT.levaRoka) { //druga crka tudi desna roka
                                        if (aT.prst > bT.prst) { //rolling desna > levi
                                            if (!cT.levaRoka) { //tretja crka tudi v desni roki
                                                if (aT.prst < bT.prst) { //navzven
                                                    if (cT.prst > bT.prst) {//monotono navzven
                                                        fE = vsiRazlicni;
                                                    }
                                                    else { //rolling na zadnjih dveh crkah
                                                        fE = razlicniRolling;
                                                    }
                                                }
                                                else {//zagotovo rolling
                                                    if (cT.prst < bT.prst) {//monotono navznoter
                                                        fE = vsiRazlicni;
                                                    }
                                                    else {//rolling
                                                        fE = razlicniRolling;
                                                    }
                                                }
                                            }
                                            else {// tretja crka v levi roki, torej monotono desna > levi
                                                fE = vsiRazlicni;
                                            }
                                        }
                                        else { //ni rollinga leva > desni
                                            if (!cT.levaRoka) { //tudi zadnja crka na desni roki
                                                if (cT.prst < bT.prst) { //monotono leva > desni
                                                    fE = vsiRazlicni;
                                                }
                                                else { // rolling po zadnjih dveh crkah
                                                    fE = razlicniRolling;
                                                }
                                            }
                                            else { //nemonotono
                                                fE = razlicniNemonotoni;
                                            }
                                        }
                                    }
                                    else { //druga crka leva roka
                                        if (cT.levaRoka) { //tretja crka tudi v levi roki
                                            if (cT.prst > bT.prst) { //monotono desna > levi
                                                fE = vsiRazlicni;
                                            }
                                            else { //rolling v levi roki
                                                fE = razlicniRolling;
                                            }
                                        }
                                        else { //tretja crka v desni roki, torej izmenjajoce - nemonotono
                                            fE = razlicniNemonotoni;
                                        }
                                    }
                                }
                            }

                            else {
                                //opcije
                                //1 - pol pol, ponavljanje
                                //4 - pol pol, izmenjaje
                                //6 - pol pol, zapored

                                if (bT.Equals(cT)) { //zadnji dve crki so isti
                                    fE = polPolPonavljanje;
                                }
                                else if (aT.prst == cT.prst && aT.levaRoka == cT.levaRoka) { //prva in zadnja crka se napisejo z istim prstom
                                    fE = polPolIzmenjaje;
                                }
                                else { //ce ni ne 1 ne 4 mora bit 6
                                    fE = polPolZapored;
                                }
                            }
                        }
                        else { //prva dva enaka
                            //opcije
                            //1 - pol pol, ponavljanje
                            //5 - vsi isti, ponavljanje
                            //6 - pol pol, ponavljajoci zapored
                            //7 - vsi isti, brez ponavljanja

                            if (aT.Equals(bT) && (cT.prst != aT.prst || cT.levaRoka != aT.levaRoka)) { //prva crka se ponovi, zadnja na drugem prstu
                                fE = polPolPonavljanje;
                            }
                            else if (aT.Equals(bT) || bT.Equals(cT)) { //bodisi se prve dve crke ponovijo, in je zadnja na istem prstu (drugace bi se sprozil prejsnji if) ali pa se zadnje dve ponovijo
                                fE = vsiIstiPonavljanje;
                            }
                            else if (aT.prst == cT.prst && aT.levaRoka == cT.levaRoka) { //vsi trije prsti ista roka
                                fE = vsiIstiBrezPonavljanja;
                            }
                            else {
                                fE = polPolPonavljanje;
                            }
                        }



                        if (fE == -1) {
                            Console.WriteLine("Prišlo je do nepredvidene napake v kodi. ");
                            Console.WriteLine(aT.idx + " " + bT.idx + " " + cT.idx);
                            Console.WriteLine(fE);
                            throw new Exception("napaka v kodi");
                        }


                        fingerEffort[a,b,c] = fE * p.fingerEffortMultiplier;
                        strokeEffort[a,b,c] = (handEffort[a,b,c] + rowEffort[a,b,c] + fingerEffort[a,b,c]) * p.strokeEffortMultiplier;

                        effortTriad[a,b,c] =
                            penaltyEffort[a,b,c] +
                            baseEffort[a,b,c] +
                            strokeEffort[a,b,c];

                        sum += effortTriad[a,b,c] * steviloTriad[a,b,c];
                        skupnoSteviloTriad += steviloTriad[a,b,c];

                        //za debug
                        sumPenalty += penaltyEffort[a,b,c];
                        sumBaseEffort += baseEffort[a,b,c];
                        sumStroke += strokeEffort[a,b,c];
                        sumHandEffort += handEffort[a,b,c];
                        sumRowEffort += rowEffort[a,b,c];
                        sumFingerEffort += fingerEffort[a,b,c];

                    }
                }
            }

            this.fitnes = (float)(sum / skupnoSteviloTriad);


            //statistika
            averagePenaltyEffort = (float)sumPenalty / (steviloTriad.Length - stPraznihTriad);
            averageBaseEffort = (float)sumBaseEffort / (steviloTriad.Length - stPraznihTriad);
            averageStrokeEffort = (float)sumStroke / (steviloTriad.Length - stPraznihTriad);
            averageHandEffort = (float)sumHandEffort / (steviloTriad.Length - stPraznihTriad);
            averageRowEffort = (float)sumRowEffort / (steviloTriad.Length - stPraznihTriad);
            averageFingerEffort = (float)sumFingerEffort / (steviloTriad.Length - stPraznihTriad);

            //IzpisiStatistiko();

        }

        public void IzpisiStatistiko() {
            Console.WriteLine();
            Console.WriteLine("Fitnes: " + this.fitnes);
            Console.WriteLine("avg base effort: " + averageBaseEffort);
            Console.WriteLine("avg penalty effort: " + averagePenaltyEffort);
            Console.WriteLine("avg stroke effort: " + averageStrokeEffort);
            Console.WriteLine("avg hand effort: " + averageHandEffort);
            Console.WriteLine("avg row effort: " + averageRowEffort);
            Console.WriteLine("avg finger effort: " + averageFingerEffort);
            Console.WriteLine();
        }

        /// <summary>
        /// Vrne indeks tipke na tipkovnici za iskano crko.
        /// </summary>
        /// <param name="a">Predstavlja crko. Najprej iskane crke, za tem fiksirane crke. </param>
        /// <param name="u">Ureditev s katero delamo. Potrebna za indekse fiksiranih tipk. </param>
        /// <returns>Indeks tipke na tipkovnici, kot definirana v ureditvi. </returns>
        public short PridobiIdxTipke(short a, Ureditev u) {
            //Ce je a vecji od stevila iskanih crk, potem je to indeks ene izmed fiksnih crk
            if(a >= u.setCrk.Length) {
                return (short)(a - u.setCrk.Length);
            }

            for(int i = 0; i < tipkovnica.Length; i++) {
                if (tipkovnica[i] == a)
                    return (short)i;
            }

            //Se ne bi smelo zgoditi
            Console.WriteLine("ERROR: Za iskano crko nisem nasel primerne tipke! ");
            Console.WriteLine(a);
            return -1;
        }

        public short[] IzreziArray(short[] org, int n) {
            short[] a = new short[n];
            for(int i = 0; i < n; i++) {
                a[i] = org[i];
            }
            return a;
        }

        //Ustvari array z zaporednimi stevilkami od 0 do stZaporednihStevil in dopolni do dolzinaArraya z -1
        public short[] ArrayZaporednihStevil(int stZaporednihStevil, int dolzinaArraya) {
            short[] a = new short[dolzinaArraya];
            for(int i = 0; i < stZaporednihStevil; i++) {
                a[i] = (short) i;
            }
            for(int i = stZaporednihStevil; i < dolzinaArraya; i++) {
                a[i] = -1;
            }

            return a;
        }

        public short[] PremesajArray(short[] a) {
            Random r = new Random();
            for(int i = 0; i < a.Length; i++) {
                int j = r.Next(i, a.Length - 1);
                short temp = a[i];
                a[i] = a[j];
                a[j] = temp;
            }

            return a;
        }

        public int CompareTo(Kromosom b) {
            return this.fitnes.CompareTo(b.fitnes);
        }

        public override string ToString() {
            return fitnes.ToString();
        }
    }

    public class Ureditev {
        public char[] setCrk; //crke katerih pozicije program isce
        public float[] baseEffort; //izracunan osnovni effort za vsako tipko glede na razdaljo tipke od domace tipke uporabljenega prsta
        public Tipka[] tipke; //reprezentacija tipk na tipkovnici
        public int[] prsti; //array dolzine 8, na kateri tipki pocivajo prsti
        public short stVrstic;
        public short[] stTipkVVrstici;
        public Tipka[] fiksneTipke;
        public bool stevilke; //ali se za ureditev isce tudi pozicije stevilk
        public bool locila; //ali se za ureditev isce tudi pozicije locil


        /* Funkcija, ki iz podanega fila na disku prebere ureditev tipk na tipkovnici in crke, katere premikamo. 
         * Dokument je formatiran kot opisano spodaj.
         * 
         * # vrstice, ki se zacnejo z '#' so ignorirane
         * # prva vrstica dokumenta vsebuje crke katere premikamo locene z vejicami
         * q,w,e,r,t,z,u,i,o,p,š,a,s,d,f,g,h,j,k,l,'c,'z,y,x,c,v,b,n,m
         * # st vrstic na tipkovnici
         * 3
         * # st tipk v vsaki vrstici, loceno z vejico
         * 12,12,11
         * # prazna vrstica
         * 
         * # vsaka tipka posebej
         * # indeks tipke
         * 0
         * # pozicije so podane v milimetrih gledano od levega spodnjega roba leve ctrl tipke - x-desno, y-gor, z-navpično (pri nepritisnjeni tipki)
         * # x pozicija
         * 40
         * # y pozicija
         * 65
         * # z pozicija
         * 9
         * # sirina tipke (torej x dimenzija) v milimetrih
         * 12
         * # visina tipke (y dimenzija) v milimetrih
         * 14
         * # roka s katero se tipko pritisne - L ali R
         * L
         * # prst s katerim se pritisne tipko 
         * # (mezinec = 5, prstanec = 4, sredinec = 3, kazalec = 2, palec = 1)
         * 5
         * # base key effort - osebno prilagojena utez ki ponazarja napor pri pritisku posamezne tipke.
         * # float stevilka od 0 do 10
         * 4.0
         * # prazna vrstica
         * 
         * # naslednja tipka
         * # .
         * # .
         * # .
         * */
        public bool PreberiUreditev(string file) {
            Console.WriteLine("Zacenjam z branjem ureditve tipkovnice. ");
            List<Tipka> tipke = new List<Tipka>();
            List<Tipka> fiksneTipke = new List<Tipka>();

            string[] vrstice = null;

            try {
                vrstice = System.IO.File.ReadAllLines(file);
            }
            catch {
                Console.WriteLine("Napaka pri odpiranju datoteke z ureditvijo. ");
                return false;
            }

            vrstice = Utilities.IzbrisiKomentarje(vrstice);

            int i = 0;

            //preberem iskane crke
            string[] crkePosamicno = vrstice[i].Split(' ');
            this.setCrk = new char[crkePosamicno.Length];
            for(int j = 0; j < crkePosamicno.Length; j++) {
                if(crkePosamicno[j].Length > 1) {
                    Console.WriteLine("Iskane crke so narobe definirane. Eden izmed vnosov je vec kot ena crka. ");
                    return false;
                }
                    
                this.setCrk[j] = crkePosamicno[j][0];
            }
            Console.WriteLine("Prebral iskane crke. ");

            i++;
            this.stVrstic = Int16.Parse(vrstice[i]);
            this.stTipkVVrstici = new short[stVrstic];
            i++;
            string[] stTipkVVrsticiPosamicno = vrstice[i].Split(',');
            if (stTipkVVrsticiPosamicno.Length != stVrstic) {
                Console.WriteLine("Napaka v formatu. Napacno stevilo navedenih vrstic. ");
            }
            for (int j = 0; j < stTipkVVrsticiPosamicno.Length; j++) {
                stTipkVVrstici[j] = Int16.Parse(stTipkVVrsticiPosamicno[j]);
            }
            Console.WriteLine("Prebral stevila tipk v vrstici. ");

            i++;
            //preskocim prazno vrstico
            if (vrstice[i].Length != 0) {
                Console.WriteLine("Napaka v formatu tipkovnice, manjkajoca prazna vrstica. ");
                return false;
            }
            else {
                i++;
            }


            int firstIdx = -1;
            int lastIdx = -1;
            //prebiram tipke
            while (true) {
                int idx = 0, x = 0, y = 0, z = 0, sizeX = 0, sizeY = 0;
                bool levaRoka = false;
                short prst = 0;
                float baseEffort = 0.0f;

                

                //preverim ce je bila prejsnja prebrana crka zadnja
                if(vrstice[i].Length == 0) {
                    break;
                }

                //preberem idx
                try {
                    idx = Int32.Parse(vrstice[i]);
                    if(firstIdx == -1) {
                        firstIdx = idx;
                        if(firstIdx != 0) {
                            Console.WriteLine("Napaka v indeksiranju crk. zacnite z 0. ");
                            return false;
                        }
                    }

                    if(lastIdx != idx - 1) {
                        Console.WriteLine("Napaka v indeksiranju crk. indeksi si ne sledijo po vrsti. ");
                        return false;
                    }

                    lastIdx = idx;
                }
                catch (FormatException) {
                    Console.WriteLine("Napaka v formatu tipkovnice, pricakoval int. ");
                }

                //preberem x
                i++;
                try {
                    x = Int32.Parse(vrstice[i]);
                }
                catch (FormatException) {
                    Console.WriteLine("Napaka v formatu tipkovnice, pricakoval int. ");
                    return false;
                }

                //preberem y
                i++;
                try {
                    y = Int32.Parse(vrstice[i]);
                }
                catch (FormatException) {
                    Console.WriteLine("Napaka v formatu tipkovnice, pricakoval int. ");
                    return false;
                }

                //preberem z
                i++;
                try {
                    z = Int32.Parse(vrstice[i]);
                }
                catch (FormatException) {
                    Console.WriteLine("Napaka v formatu tipkovnice, pricakoval int. ");
                    return false;
                }

                //preberem sizeX
                i++;
                try {
                    sizeX = Int32.Parse(vrstice[i]);
                }
                catch (FormatException) {
                    Console.WriteLine("Napaka v formatu tipkovnice, pricakoval int. ");
                    return false;
                }

                //preberem sizeY
                i++;
                try {
                    sizeY = Int32.Parse(vrstice[i]);
                }
                catch (FormatException) {
                    Console.WriteLine("Napaka v formatu tipkovnice, pricakoval int. ");
                    return false;
                }

                //preberem roko
                i++;
                if(vrstice[i][0] == 'L') {
                    levaRoka = true;
                }
                else if(vrstice[i][0] == 'R') {
                    levaRoka = false;
                }
                else {
                    Console.WriteLine("Napaka v formatu tipkovnice, pricakoval L ali R. ");
                    return false;
                }

                //preberem prst
                i++;
                try {
                    prst = Int16.Parse(vrstice[i]);
                }
                catch (FormatException) {
                    Console.WriteLine("Napaka v formatu tipkovnice, pricakoval int. ");
                    return false;
                }

                //preberem baseEffort
                i++;
                try {
                    baseEffort = float.Parse(vrstice[i]);
                }
                catch (FormatException) {
                    Console.WriteLine("Napaka v formatu tipkovnice, pricakoval float. ");
                    return false;
                }

                Tipka t = new Tipka(idx, x, y, z, sizeX, sizeY, levaRoka, prst, baseEffort);
                //pogledam, ce je fiksna tipka
                i++;
                if(vrstice[i].Length != 0) {
                    if (vrstice[i].StartsWith("fiksirana ")) {
                        string preostanek = vrstice[i].Substring(10);
                        Console.WriteLine("Fiksirana tipka: " + preostanek);
                        if(preostanek.Length == 1) {
                            t.vrednost = preostanek[0];
                        }
                        else {
                            if (preostanek.Equals("presledek")) {
                                t.vrednost = ' ';
                            }
                            else if (preostanek.Equals("enter")) {
                                t.vrednost = '\n';
                            }
                            else {
                                Console.WriteLine("Nepoznana vrednost za fiksirano tipko. Poglejte, če je za vrednostjo morda presledek. ");
                                return false;
                            }

                            i++;
                            if (vrstice[i].Length != 0) {
                                Console.WriteLine("Napaka v formatu tipkovnice, pricakoval prazno vrstico. ");
                                return false;
                            }
                            else {
                                i++;
                            }
                        }
                        fiksneTipke.Add(t);
                    }
                    else {
                        Console.WriteLine("Napaka v formatu tipkovnice, pricakoval prazno vrstico ali ukaz \"fiksirana\". ");
                        return false;
                    }
                }
                else {
                    i++;
                    tipke.Add(t);
                }
                Console.WriteLine("Prebral tipko " + t.idx);
            }

            this.tipke = tipke.ToArray();
            this.fiksneTipke = fiksneTipke.ToArray();
            bool presledek = false;
            bool enter = false;
            foreach(Tipka t in fiksneTipke) {
                if (t.vrednost == ' ')
                    presledek = true;
                else if (t.vrednost == '\n')
                    enter = true;
            }

            if(!presledek || !enter) {
                Console.WriteLine("V ureditvi nista definirana presledek in enter! ");
                return false;
            }

            Console.WriteLine("Prebral tipke. Stevilo prebranih tipk: " + (this.tipke.Length) + " Stevilo prebranih fiksiranih tipk: " + (this.fiksneTipke.Length));

            //preberem indekse prstov
            i++;
            string[] prstiString = vrstice[i].Split(' ');
            prsti = new int[8];
            if (prstiString.Length != 8)
                Console.WriteLine("Napaka pri indeksiranju prstov, napisanih mora biti natanko 8 indeksov. ");

            for(int k = 0; k < 8; k++) {
                try {
                    prsti[k] = Int32.Parse(prstiString[k]);
                }
                catch {
                    Console.WriteLine("Napaka pri indeksiranju prstov. ");
                    return false;
                }
            }

            Console.WriteLine("Prebral indekse prstov. ");
            Console.WriteLine("Uspešno prebran file ureditve tipkovnice. ");
            IzracunajBaseEffort();
            Console.WriteLine("Uspešno izračunal base effort dane tipkovnice. ");

            foreach (char c in setCrk) {
                if (char.IsNumber(c)){
                    stevilke = true;
                    if (stevilke && locila)
                        break;
                }
                else if (char.IsPunctuation(c)) {
                    locila = true;
                    if (stevilke && locila)
                        break;
                }
            }

            if(!stevilke || !locila) {
                foreach(Tipka t in fiksneTipke) {
                    if (char.IsNumber(t.vrednost)){
                        stevilke = true;
                        if (stevilke && locila)
                            break;
                    }
                    else if (char.IsPunctuation(t.vrednost)) {
                        locila = true;
                        if (stevilke && locila)
                            break;
                    }
                }
            }

            Console.WriteLine();
            if (stevilke) {
                Console.WriteLine("V iskanje so vključene tudi številke. ");
            }
            else {
                Console.WriteLine("V iskanje številke niso vključene. ");
            }

            if (locila) {
                Console.WriteLine("V iskanje so vključena tudi ločila. ");
            }
            else {
                Console.WriteLine("V iskanje ločila niso vključena. ");
            }

            return true;
        }


        //za vsako tipko izracuna razdaljo do domace tipke za prst, ki se uporablja pri pritisku te tipke
        //zaenkrat se racuna razdalja do sredisca tipke, bi se pa to lahko se spremenilo, glede na to, da imamo podatke tudi o velikosti tipk
        void IzracunajBaseEffort() {

            foreach(Tipka t in tipke) {
                BaseEffort(t);
            }

            foreach(Tipka t in fiksneTipke) {
                if(t.vrednost == ' ') { //za presledek se implicira effort 0
                    continue;
                }
                BaseEffort(t);
            }

            //izpis za debug
            Console.WriteLine("Izracunani base efforti: ");
            int idxTipke = 0;
            for (int i = 0; i < stVrstic; i++) {
                for (int j = 0; j < stTipkVVrstici[i]; j++) {
                    Console.Write(tipke[idxTipke].baseEffort + ", ");
                    idxTipke++;
                }
                Console.WriteLine();
            }
            foreach(Tipka t in fiksneTipke) {
                if(t.vrednost == ' ') {
                    Console.Write("Presledek: ");
                }
                else if(t.vrednost == '\n') {
                    Console.Write("Enter: ");
                }
                else {
                    Console.Write(t.vrednost + ": ");
                }
                Console.WriteLine(t.baseEffort);
            }

            Console.WriteLine("Penalty efforti: ");
            idxTipke = 0;
            for (int i = 0; i < stVrstic; i++) {
                for (int j = 0; j < stTipkVVrstici[i]; j++) {
                    Console.Write(tipke[idxTipke].penaltyEffort + ", ");
                    idxTipke++;
                }
                Console.WriteLine();
            }
            foreach (Tipka t in fiksneTipke) {
                if (t.vrednost == ' ') {
                    Console.Write("Presledek: ");
                }
                else if (t.vrednost == '\n') {
                    Console.Write("Enter: ");
                }
                else {
                    Console.Write(t.vrednost + ": ");
                }
                Console.WriteLine(t.penaltyEffort);
            }

        }

        //Izracuna base effort za dano tipko
        private void BaseEffort(Tipka a) {
            // da dobimo idx iskanega prsta v tabeli prsti:
            //"preslikat" je treba:
            //    L    /    R
            // 5 4 3 2   2 3 4 5
            // v
            // 0 1 2 3 4 5 6 7
            short p = a.prst;
            if (!a.levaRoka)
                p += 2;
            else
                p = (short)(5 - p);

            int domacaTipkaPrsta = prsti[p];
            Tipka b = tipke[domacaTipkaPrsta];

            //imamo obe tipke, izracunat je treba se razdaljo od a do b
            float dx = (a.x + a.sizeX / 2.0f) - (b.x + b.sizeX / 2.0f);
            float dy = (a.y + a.sizeY / 2.0f) - (b.y + b.sizeY / 2.0f);
            float dz = a.z - b.z;

            float d = (float)(Math.Sqrt(dx * dx + dy * dy + dz * dz));
            //ker racunamo v mm se delim, da dobimo bolj normalne stevilke
            d = d / 10;
            d = (float)Math.Round(d, 1);

            a.baseEffort = d;
        }

        public void IzpisiUreditev(Kromosom k) {
            //Console.WriteLine("Najboljsa ureditev: ");
            int idxCrke = 0;
            for(int i = 0; i < stVrstic; i++) {
                for(int j = 0; j < stTipkVVrstici[i]; j++) {
                    if (k.tipkovnica[idxCrke] == -1)
                        Console.Write("_, ");
                    else
                        Console.Write(setCrk[k.tipkovnica[idxCrke]] + ", ");
                    idxCrke++;
                }
                Console.WriteLine();
            }
            
        }

        public short Vrednost(char c) {
            for(int i = 0; i < setCrk.Length; i++) {
                if (setCrk[i] == c)
                    return (short)i;
            }

            for (int i = 0; i < fiksneTipke.Length; i++) {
                if(c == fiksneTipke[i].vrednost) {
                    return (short)(setCrk.Length + i);
                }
            }

            return -1;
        }

        /// <summary>
        /// Vrne tipko na danem indeksu.
        /// </summary>
        /// <param name="idx">Indeks iskane tipke</param>
        /// <returns>Tipka na danem indeksu</returns>
        public Tipka PridobiTipko(short idx) {
            if (idx >= tipke.Length) {
                return fiksneTipke[idx - tipke.Length];
            }
            else
                return tipke[idx];
        }
    }

    public class Tipka {
        public int idx;
        public int x;
        public int y;
        public int z;
        public int sizeX;
        public int sizeY;
        public bool levaRoka;
        public short prst; //5-mezinec, 1-palec
        //Penalty effort ni (kot opisano na carpalxu) sestevek hand row in finger cost, 
        //ampak je definiran s strani posameznika za vsako tipko posebej. 
        //Tudi na carpalxu se definira te utezi posebej subjektivno, torej ne zgubljam na objektivnosti
        //Sprememba je potrebna zaradi alternativnih oblik tipkovnic, ki niso nujno razporejene v vrste
        public float penaltyEffort;
        //Base effort je izracunan glede na distanco
        public float baseEffort;
        public char vrednost;

        public Tipka(int idx, int x, int y, int z, int sizeX, int sizeY, bool levaRoka, short prst, float penaltyEffort) {
            this.idx = idx;
            this.x = x;
            this.y = y;
            this.z = z;
            this.sizeX = sizeX;
            this.sizeY = sizeY;
            this.levaRoka = levaRoka;
            this.prst = prst;
            this.penaltyEffort = penaltyEffort;

            return;
        }
    }
}
